
let hash_map = {}

const alphabetize = word => word.toLowerCase().split("").sort().join("").trim()

palavras.forEach(w => {
    const key = alphabetize(w)

    if(hash_map[key]) hash_map[key].push(w)
    else hash_map[key] = [w]
})

const getSetsOfFiveAnagrams = (min=5) => {
    let arr = []

    for (key in hash_map) {
        hash_map[key] = [...(new Set(hash_map[key].
                        map(w => w.toLowerCase()).
                        map(w => w[0].toUpperCase() + w.slice(1))))]

        const hash_item = hash_map[key]
        
        if (hash_item.length >= min) arr.push(hash_item)
    }

    return arr
}

// Bonus 1
const bonus_1_area = document.querySelector('section.bonus_1')
const anagrams = getSetsOfFiveAnagrams().sort((a, b) => a.length - b.length)
const bonus_1_aside = document.querySelector('aside.aside')

anagrams.forEach(anagram_arr => {
    const section = document.createElement('section')

    anagram_arr.forEach((word, index) => {
        const paragraph = document.createElement('p')

        if (index === 0) {
            const title = document.createElement('h3')
            const id = alphabetize(word)
            const link = document.createElement('a')

            title.innerText = id
            section.setAttribute('id', id)
            section.appendChild(title)
            link.setAttribute('href', `#${id}`)
            link.innerText = id
            bonus_1_aside.appendChild(link)
        }
        paragraph.innerText = word
        section.appendChild(paragraph)
    })
    bonus_1_area.appendChild(section)
})

bonus_1_aside.addEventListener('click', evt => {
    const anchor = evt.target
    if (anchor.tagName.toLowerCase() === 'a') {
        const sec_id = anchor.innerText
        const section = document.getElementById(sec_id)

        section.classList.add('change_color')
        setTimeout( _ => {section.classList.remove('change_color')}, 2000)
    }
})

// Bonus 2

const bonus_2_button = document.querySelector('button.bonus_2__input_button')

const parse_input = input => input.replace(/,|!|\.|:|\?|\[|]|\\|\/|;|-|_|\+/g, ' ').split(' ')
const bonus_2_out_area = document.querySelector('section.bonus_2__results')

let used_words = {}

const permute_2 = (str, i=0) => {
    if (i === str.length - 1) {
        for (let j = 1; j < str.length; j++) {
            const prefix = str.slice(0, j)
            const sufix = str.slice(j)
            
            if (hash_map[prefix]
                && hash_map[sufix] 
                && !(used_words[prefix + sufix] 
                || used_words[sufix + prefix])
            ) {
                const two_anagrams = `${hash_map[prefix].join(',')} + ${hash_map[sufix].join(',')}`
                const p = document.createElement('p')

                p.innerText = two_anagrams
                bonus_2_out_area.appendChild(p)
                used_words[prefix + sufix] = true
            }
        }

        return
    }
   
    let prev = '*';

    for (let j = i; j < str.length; j++) {
        let temp = str
        const aux = temp[j]

        if (j > i && temp[i] === temp[j]) continue
        if (prev !== '*' && prev === str[j]) continue
        temp = temp.split('')
        temp[j] = temp[i]
        temp[i] = aux
        temp = temp.join('')
        prev = str[j];
        permute_2(temp, i + 1)
    }
}

bonus_2_button.addEventListener('click', _ => {
    const input = parse_input(document.getElementById('bonus_2_input').value).join('')
    const parsed = alphabetize(input)
    const h3 = document.createElement('h3') 

    used_words = {}
    bonus_2_out_area.innerHTML = ''
    h3.innerText = 'Anagrams Found:'
    bonus_2_out_area.appendChild(h3)
    permute_2(parsed)
})

// Bonus 3

const bonus_3_button = document.querySelector('button.bonus_3__input_button')
const bonus_3_out_area = document.querySelector('section.bonus_3__results')

const permute_3 = (str, i=0) => {
    if (i === str.length - 1) {
        console.log(str)
        for (let j = 1; j < str.length; j++) {
            const prefix = str.slice(0, j)
            
            for (let k = j + 1; k < str.length; k++) {
                const sub_word = str.slice(j, k)
                const sufix = str.slice(k)

                if (hash_map[prefix]
                    && hash_map[sub_word]
                    && hash_map[sufix]
                    && !(used_words[prefix + sub_word + sufix] 
                    || used_words[prefix + sufix + sub_word]
                    || used_words[sufix + sub_word + prefix]
                    || used_words[sufix + prefix + sub_word]
                    || used_words[sub_word + prefix + sufix]
                    || used_words[sub_word + sufix + prefix])
                ) {
                    const three_anagrams = `${hash_map[prefix].join(', ')} + ${hash_map[sub_word].join(', ')} + ${hash_map[sufix].join(', ')}`
                    const p = document.createElement('p')
    
                    p.innerText = three_anagrams
                    bonus_3_out_area.appendChild(p)
                    used_words[prefix + sub_word + sufix] = true
                }
            }
        }
        return
    }
   
    let prev = '*';

    for (let j = i; j < str.length; j++) {
        let temp = str
        const aux = temp[j]

        if (j > i && temp[i] === temp[j]) continue
        if (prev !== '*' && prev === str[j]) continue
        temp = temp.split('')
        temp[j] = temp[i]
        temp[i] = aux
        temp = temp.join('')
        prev = str[j];
        permute_3(temp, i + 1)
    }
}

bonus_3_button.addEventListener('click', _ => {
    const input = parse_input(document.getElementById('bonus_3_input').value).join('')
    const parsed = alphabetize(input)
    const h3 = document.createElement('h3') 

    used_words = {}
    bonus_3_out_area.innerHTML = ''
    h3.innerText = 'Anagrams Found:'
    bonus_3_out_area.appendChild(h3)
    permute_3(parsed)
})